"""
Lib Inkombizz 1.0
"""
# built-in modules
import sys
import string
from . import constants
from . import exceptions
from . import base

# Check python version
if sys.version_info[:3] < (2, 4, 0):
	raise ImportError("Lib Inkombizz must be installed new version of python, %s, is too old. 2.4 or higher is required" % string.split(sys.version)[0])
version = "1.0"
