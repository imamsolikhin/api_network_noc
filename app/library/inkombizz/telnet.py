"""
LibInkombizz 1.0
"""

# builtin modules
import telnetlib
import types
import logging
import time
from . import exceptions
from . import base
from . import emulate
import socket
import re
from datetime import datetime

def ParseSection(block):
	regex = {}
	regex['unix'] = "\u001b||\\[37D||More ( Press 'Q' to break )||----||----\u001b||\u0007||More ( Press 'Q' to break ) \u001b                                     \u001b"
	regex['split_char'] = '\r'
	regex['space'] = ' +'
	regex['perfix'] = r"""(?:"|>") %(unix)s|%(split_char)s""" % regex
	output = re.sub(regex['perfix'], '', block)
	# output = re.sub(regex['space'], ' ', output)
	ansi_escape = re.compile(r'''\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])''', re.VERBOSE)
	output = ansi_escape.sub('', output)
	return output

def ParseSectionBlock(block):
	regex = {}
	regex['name'] = '[a-zA-Z0-9]+'
	regex['safe_char'] = '[^";:,]'
	regex['qsafe_char'] = '[^"]'
	regex['param_value'] = r"""(?:"|\\") %(qsafe_char)s * (?:"|\\") | %(safe_char)s + """ % regex
	regex['param'] = r""" (?: %(name)s ) = (?: %(param_value)s )? """ % regex

	properties = {}
	for m in re.findall(regex['param'], block, re.VERBOSE):
		(name, value) = m.split("=")
		value = re.sub(r'(^\\"|\\"$)', '', value)
		properties[name.lower()] = value

	return properties

class Telnet(base.BaseIOInput):
	terminal	= None
	hostname	= None
	password 	= None
	port 		= 23
	hasecho 	= False
	sysname 	= None
	output		= None
	status		= False

	def __init__(self, hostname=None, timeout=None, username=None, password=None, sysname=None):
		self.hostname = hostname
		if timeout:
			self.timeout = timeout
		if username:
			self.username = username
		if password:
			self.password = password
		if sysname:
			self.sysname = sysname

	def isstatus(self):
		return self.status

	def connect(self):
		try:
			self.terminal = telnetlib.Telnet(self.hostname,self.port)
		except socket.error:
			self.status = True
			self.output = "Connection failed to host ('telnet %s %s')" % (self.hostname, self.port)
		self.output = "Connection success to host ('telnet %s:%s')" % (self.hostname, self.port)
		time.sleep(0.1)

	def disconnect(self):
		if self.terminal:
			self.terminal.write("quit".encode("ascii")+b"\n")
			time.sleep(0.1)
			self.terminal.write("y".encode("ascii")+b"\n")
			self.terminal.close()
			self.terminal = None

	def login(self):
		try:
			self.terminal = telnetlib.Telnet(self.hostname,self.port,5)
		except socket.error:
			self.terminal = None
			self.status = True
			self.output = "Connection failed to host ('%s %s')" % (self.hostname, self.port)

		if self.terminal:
			# self.terminal.set_debuglevel(200)
			# print(telnetlib.IAC + telnetlib.WILL + telnetlib.ECHO)
			self.terminal.write(telnetlib.IAC + telnetlib.DONT + telnetlib.ECHO)
			self.terminal.read_until(b"name:")
			self.terminal.write(self.username.encode("ascii")+b"\n")
			time.sleep(0.1)
			if self.terminal:
				self.terminal.read_until(b"password:")
				self.terminal.write(self.password.encode("ascii")+b"\n")
				time.sleep(0.2)
				self.terminal.write(" ".encode("ascii")+b"\n")
				time.sleep(0.2)
				self.terminal.write(" ".encode("ascii")+b"\n")
				time.sleep(0.2)
				self.valdlogin()

	def valdlogin(self,auth=None):
		self.output = ParseSection(self.terminal.read_eager().decode("ascii"))
		if "invalid" in self.output:
			self.terminal = None
			self.status = True
			self.output = "Username or Password is not valid"
			if auth:
				self.output = str(auth)+" is not valid"
		else:
			self.status = False
			self.output = "Username or password is valid"

	def logout(self):
		if self.terminal:
			self.terminal.write("quit".encode("ascii")+b"\n")
			time.sleep(0.1)
			self.terminal.write("y".encode("ascii")+b"\n")
			self.terminal.close()
			self.terminal = None

	def get_result(self, val=None):
		if val:
			result = ParseSection(val)
		else:
			result = ParseSection(self.output)
		return result

	def get_result_json(self, val=None, line=9999):
		result = []
		if val:
			data = ParseSection(val).split("\n")
			result.append(data[0])
			i = 0
			for rs in data:
				if rs != result[i]:
					i = i+1
					# print(rs)
					result.append(rs)
					if i > line:
						break
		else:
			data = ParseSection(self.output).split("\n")
			result.append(data[0])
			i = 0
			for rs in data:
				if rs.replace(" ", "") != result[i].replace(" ", "") and rs.replace(" ", "") != "":
					# print(rs)
					i = i+1
					result.append(rs)

				if i > line:
					break
		return result

	def cmd(self, command, sleep=None, stop=None):
		if self.terminal:
			self.terminal.write(b""+command.encode("ascii"))
			if stop == None:
				time.sleep(1)
				self.terminal.write(b" ")

			# time.sleep(round(len(command)/25,2))
			if sleep:
				sleep = sleep
			else:
				sleep = round(len(command)/50,2)

			start_sec = datetime.now().replace(microsecond=0)
			self.runcmd(sleep,stop)
			end_sec = datetime.now().replace(microsecond=0)
			duration = end_sec-start_sec
			totalminute = int(duration.total_seconds())
			# print(str(command)+" == "+str(sleep)+" ==> "+str(totalminute)+" Second")

	def runcmd(self,sleep,stop):
		self.output	= ""
		if self.terminal:
			content = self.terminal.read_very_eager().decode("ascii")
			counter = 0
			same_content = ""
			while True:
				content = ParseSection(content)
				data = content.split("\n")
				# print(str(counter)+" == "+str(len(data))+" <==> ["+str(same_content)+" == "+str(data[len(data)-1])+"]")
				if same_content == re.sub(' ', '',data[len(data)-1]):
					counter = counter+1
					sleep = 0.2
				if counter > 3:
					break

				same_content = re.sub(' ', '',data[len(data)-1])
				self.output = str(self.output) + str(content)

				self.terminal.write(" ".encode("ascii")+b"\n")
				time.sleep(sleep)
				content = self.terminal.read_very_eager().decode("ascii")
				# print(content)
