# -*- coding: utf-8 -*-
"""Internal module of protocols with abstract base classes.
Applications should not directly import this module, expect to make subclasses.
As a reminder: all internal strings, like identifier, should be
represented in UTF-8. Use pynt.xmlns.UTF8 if you need help converting."""

# builtin modules
import types
import logging
import time
import threading
from . import exceptions

class BaseIOInput(object):
	timeout     	= 2
	port     		= 23
	terminator  	= "\r\n"
	prompt      	= ">"
	delimiter   	= "\r\n>"
	logfile     	= None
	autocallbacks   = None
	status        	= False
	response        = None
	message			= None

	def __init__(self):
		"""Prepares the actual underlying I/O, given the parameters given at initialization.
		(e.g. hostname, port, filename, url, File object). If possible, delays the actual
		opening of the I/O till connect() is called, so that setLoginCredentials() can be
		called in the mean time."""
		pass

	def setDefaultTimeout(self, timeout):
		self.timeout = int(timeout)

	def setDefaultPort(self, port):
		self.port = int(port)

	def send_and_receive(self, command, timeout):
		(identifier, string) = self.makeCommand(command)
		self.sendcommand(string)
		(resultlines, status) = self.getmessage(identifier, timeout=timeout)
		self.statusOK(status, command)
		return (resultlines, status)

	def sendcommand(self, string):
		self.writetolog(string, input=True)
		logger = logging.getLogger("protocols")
		logger.debug("Sending command %s" % (repr(string)))

	def readmessage(self, timeout):
		resultString = ""
		self.writetolog(resultString, output=True)
		logger = logging.getLogger("protocols")
		logger.debug("Received %d bytes of data" % len(resultString))
		return resultString

	def makeCommand(self, command):
		return (None, command+"\n")

	def writetolog(self, logstring, input=False, output=False):
		"""Write to log file"""
		if self.logfile:
			self.acquireLoglock()
			if input:
				self.logfile.write("\n==input==\n")
			elif output:
				self.logfile.write("\n==output==\n")
			else:
				self.logfile.write("\n==i/o==\n")
			self.logfile.write(logstring)
			self.releaseLoglock()

	def getmessage(self, identifier, timeout):
		endtime = time.time() + timeout
		skipcount = 0
		logger = logging.getLogger("protocols")
		while True:
			result = self.readmessage(timeout)  # may raise a TimeOut
			(resultlines, residentifier, status) = self.parseMessage(result)
			autotype = self.isAutonomousType(residentifier, status)
			if (autotype != False):
				# Autonomous message
				if autotype in self.autocallbacks:
					callback = self.autocallbacks[autotype]
					logger.info("Sending autonomous message (type %s, identifier %s) to %s" % (autotype,residentifier,callback.__name__))
					self.callback(callback, resultlines, status)
				elif True in self.autocallbacks:   # catch-all callback function
					callback = self.autocallbacks[True]
					logger.info("Sending autonomous message (type %s, identifier %s) to %s" % (autotype,residentifier,callback.__name__))
					self.callback(callback, resultlines, status)
				else:
					logger.warning("Skipping unhandled autonomous message (type %s, identifier %s)" % (autotype,residentifier))
			elif identifier == residentifier:
				logger.debug("Got matching result for identifier %s" % identifier)
				break
			else:
				skipcount += 1
				logger.error("Skipping regular message with identifier %s" % (residentifier))
			if time.time() > endtime:
				raise exceptions.TimeOut("No reply with correct identifier %s after %d seconds (skipped %d responses)" % (identifier, timeout, skipcount))
				resultlines = []
				status = False
				break
		return (resultlines, status)
	def releaseLoglock(self):
		logger = logging.getLogger("protocols")
		logger.debug("Release log lock by thread %s" % (threading.currentThread().getName()))
		self.iolock.release()
		return True;

	def parseMessage(self, resultString):
		resultLines = resultString.split('\n');
		return (resultLines, None, True)

	def isAutonomousType(self, identifier, status):
		"""Given the identifier and status, decide if the message is autonomous,
		and if so, if it is of a certain type. For regular (non-autonomous), return False."""
		return False

	def statusOK(self, status, command):
		"""Checks the status. returns True is the status is a succesful,
		or raises a CommandFailed, possible with additional information."""
		status = bool(status)
		if not status:
			raise CommandFailed("Unexpected status '%s' from command '%s'" % (status, command))
