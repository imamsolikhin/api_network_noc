"""
Constants used by apps.
"""

# ------------ General Constants -----------------
message     = None
response    = None

# ------------ General Constants -----------------
TEXT_SUCCESS_DATA = "Data successfully"
TEXT_ERORR_DATA = "Data unsuccessfully"
TEXT_FAILED_CONNECT = "Connection Fail"
TEXT_SUCCESS_CONNECT = "Connection Success"
TEXT_ALREADY_EXISTS = "Already Exists"
TEXT_DOES_NOT_EXISTS = "Does Not Exists"
TEXT_OPERATION_SUCCESSFUL = "Data successfully saved"
TEXT_OPERATION_UNSUCCESSFUL = "Operation Unsuccessful"
TEXT_MISSING_PARAMS = "Params are missing"
INVALID_DATA = "Invalid data"
