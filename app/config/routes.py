"""
Routing urls for the Api app
"""
# third-party imports
from flask import Blueprint, render_template
from ..api.controllers.huawai import huawei

def init(app):
	app.add_url_rule("/", "home", home)
	app.add_url_rule("/api/", "api", page_api)
	app.add_url_rule("/api/v1", "api", page_api)

	app.register_blueprint(huawei)

def home():
	return render_template('index.html')

def page_api():
	return render_template('api.html')
