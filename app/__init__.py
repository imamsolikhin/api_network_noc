from flask import Flask
from .config import routes

def create_app(config):
	app = Flask(__name__,template_folder='templates')
	app.config.from_object(config)
	routes.init(app)
	return app
