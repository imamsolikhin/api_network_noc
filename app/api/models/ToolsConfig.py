from marshmallow import Schema, fields

class ToolsConfig(Schema):
    city = fields.Str()
    name = fields.Str()

tools_schema = ToolsConfig()
json_string = tools_schema.dumps(list_name, many=True)
