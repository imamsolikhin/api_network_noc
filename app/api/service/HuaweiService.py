"""
Schemas for the Service app Huawei
"""
import os, re

def CheckPower(host,data):
	result = None
	OntDistance = 0
	OntRange = 0
	RxPower = 0
	TxPower = 0
	signal = ["Rx optical power(dBm)", "Tx optical power(dBm)", "ONT distance(m)", "Rx power current alarm threshold(dBm)"]

	for x in range(0, len(data)):
		if data[x] != "":
			rs = " ".join(data[x].split(':')[0].lstrip().split())
			if signal[0] == rs:
				RxPower = data[x].split(':')[1].lstrip()
			if signal[1] == rs:
				TxPower = data[x].split(':')[1].lstrip()
			if signal[2] == rs:
				OntDistance = data[x].split(':')[1].lstrip()
			if signal[3] == rs:
				OntRange = data[x].split(':')[1].lstrip()

	return {
		"host":host,
		"tx_power":TxPower,
		"rx_power":RxPower,
		"distance":OntDistance,
		"range":OntRange
	}

def OntOpticalInfo(host,data):
	result = None
	rest = [host,"", "", "", "", ""]
	arr = [host, "Optical power precision(dBm)", "Rx optical power(dBm)", "Tx optical power(dBm)", "Temperature(C)" , "OLT Rx ONT optical power(dBm)"]

	for x in range(0, len(data)):
		if data[x] != "":
			rs = " ".join(data[x].split(':')[0].lstrip().split())
			if arr[1] == rs:
				rest[1] = data[x].split(':')[1].lstrip()
			if arr[2] == rs:
				rest[2] = data[x].split(':')[1].lstrip()
			if arr[3] == rs:
				rest[3] = data[x].split(':')[1].lstrip()
			if arr[4] == rs:
				rest[4] = data[x].split(':')[1].lstrip()
			if arr[5] == rs:
				rest[5] = data[x].split(':')[1].lstrip()

	return {
		"hostcode":rest[0],
		"optical_power_precision":rest[1],
		"olt_rx_optical_power":rest[2],
		"tx_optical_power":rest[3],
		"temperature":rest[4],
		"rx_optical_power":rest[5]
	}

def OntInfo(host,data):
	result = None
	rest = [host,"", "", "", "", "", "", ""]
	arr = [host,"Last down cause", "Last up time", "Last down time", "Last dying gasp time","ONT online duration","ONT distance(m)","Temperature"]

	for x in range(0, len(data)):
		if data[x] != "":
			rs = " ".join(data[x].split(':')[0].lstrip().split())
			if arr[1] == rs:
				rest[1] = data[x].split(':')[1].lstrip()
			if arr[2] == rs:
				rest[2] = data[x].split(':')[1].lstrip()
			if arr[3] == rs:
				rest[3] = data[x].split(':')[1].lstrip()
			if arr[4] == rs:
				rest[4] = data[x].split(':')[1].lstrip()
			if arr[5] == rs:
				rest[5] = data[x].split(':')[1].lstrip()
			if arr[6] == rs:
				rest[6] = data[x].split(':')[1].lstrip()
			if arr[7] == rs:
				rest[7] = data[x].split(':')[1].lstrip()

	return {
		"hostcode":rest[0],
		"last_down_cause":rest[1],
		"last_up_time":rest[2],
		"last_down_time":rest[3],
		"last_dying_gasp_time":rest[4],
		"ont_online_duration":rest[5],
		"ont_distance":rest[6],
		"temperature":rest[7]
	}

def OntWanInfo(host,data):
	result = None
	rest = [host,"", "", "", "", "", ""]
	arr = [host,"Service type", "Connection type", "IPv4 Connection status", "IPv4 access type", "IPv4 address", "MAC address"]

	for x in range(0, len(data)):
		if data[x] != "":
			rs = " ".join(data[x].split(':')[0].lstrip().split())
			if arr[1] == rs:
				rest[1] = data[x].split(':')[1].lstrip()
			if arr[2] == rs:
				rest[2] = data[x].split(':')[1].lstrip()
			if arr[3] == rs:
				rest[3] = data[x].split(':')[1].lstrip()
			if arr[4] == rs:
				rest[4] = data[x].split(':')[1].lstrip()
			if arr[5] == rs:
				rest[5] = data[x].split(':')[1].lstrip()
			if arr[6] == rs:
				rest[6] = data[x].split(':')[1].lstrip()

	return {
		"hostcode":rest[0],
		"service_type":rest[1],
		"connection_type":rest[2],
		"ipv4_connection_status":rest[3],
		"ipv4_access_type":rest[4],
		"ipv4_address":rest[5],
		"mac_address":rest[6]
	}

def OntPortState(host,data):
	result = []
	for x in range(0, len(data)):
		if data[x] != "":
			data_resp = re.sub(' +' , ' ' , data[x]).lstrip().split(" ")
			if len(data_resp) == 8:
				result.append({
					"host"		:host,
					"ont_id"		:data_resp[0],
					"port_id"		:data_resp[1],
					"port_type"		:data_resp[2],
					"speed"		:data_resp[3],
					"duplex"		:data_resp[4],
					"link_state"		:data_resp[5],
					"ring_status"	:data_resp[6]
				})
	if not result:
		result.append({
			"host"			:host,
			"ont_id"		:"",
			"port_id"		:"",
			"port_type"		:"",
			"speed"			:"",
			"duplex"		:"",
			"link_state"	:"",
			"ring_status"	:""
		})
	return result

def TrafficTable(host,data):
	result = []
	for x in range(0, len(data)):
		if data[x] != "":
			if "traffic table" in data[x]:
				data_resp = re.sub(' +' , ' ' , data[x]).lstrip().split(" ")
				list_traffic = ["", "", "", "", "", "", ""]
				for i in range(0, len(data_resp)):
					if data_resp[i] != "":
						if data_resp[i] == "index":
							list_traffic[0] = data_resp[i+1]
						if data_resp[i] == "name":
							list_traffic[1] = data_resp[i+1]
						if data_resp[i] == "cir":
							list_traffic[2] = data_resp[i+1]
						if data_resp[i] == "cbs":
							list_traffic[3] = data_resp[i+1]
						if data_resp[i] == "pir":
							list_traffic[4] = data_resp[i+1]
						if data_resp[i] == "pbs":
							list_traffic[5] = data_resp[i+1]
						if data_resp[i] == "priority":
							list_traffic[6] = data_resp[i+1]

				traffic = []
				for i in range(0, len(list_traffic)):
					if list_traffic[0] not in traffic:
						traffic.append(list_traffic[0])
						if list_traffic[1] != "":
							result.append({
								"host"		:host,
								"index"		:list_traffic[0],
								"name"		:list_traffic[1],
								"cir"		:list_traffic[2],
								"cbs"		:list_traffic[3],
								"pir"		:list_traffic[4],
								"pbs"		:list_traffic[5],
								"priority"	:list_traffic[6]
							})
	return result

def LineProfile(host,data):
	result = []
	for x in range(0, len(data)):
		if data[x] != "":
			if "Profile-ID" in data[x]:
				x = x+1
			data_resp = re.sub(' +' , ' ' , data[x]).lstrip().split(" ")
			if data_resp:
				if len(data_resp) == 4:
					if data_resp[0] != "display" and data_resp[0] != "{":
						result.append({
							"host"		:host,
							"id"		:data_resp[0],
							"name"		:data_resp[1],
							"binding"	:data_resp[2]
						})
	return result

def ServiceProfile(host,data):
	result = []
	for x in range(0, len(data)):
		if data[x] != "":
			if "Profile-ID" in data[x]:
				x = x+1
			data_resp = re.sub(' +' , ' ' , data[x]).lstrip().split(" ")
			if data_resp:
				if len(data_resp) == 4:
					if data_resp[0] != "display" and data_resp[0] != "{":
						result.append({
							"host"		:host,
							"id"		:data_resp[0],
							"name"		:data_resp[1],
							"binding"	:data_resp[2]
						})
	return result

def PortVlan(host,data):
	vlan_id = []
	vlan_type = []
	vlan_desc = []
	vlan_attr = []
	vlan_port = []
	result = []
	for x in range(0, len(data)):
		if data[x] != "":
			if "vlan-config" in data[x]:
				x = x+1
			if x < len(data):
				data_vald = re.sub(' +' , ' ' , data[x]).lstrip().split(" ")
				if data_vald[0] == "vlan" and data_vald[1] != "desc" and data_vald[1] != "attrib":
					if " to " in data[x]:
						i = data_vald[1]
						for i in range(int(data_vald[1]), int(data_vald[3])+1):
							vlan_id.append(i)
							vlan_type.append(data_vald[4])
							vlan_desc.append("")
							vlan_attr.append("")
							vlan_port.append("///")
					else:
						vlan_id.append(data_vald[1])
						vlan_type.append(data_vald[2])
						vlan_desc.append("")
						vlan_attr.append("")
						vlan_port.append("///")

				if data_vald[0] == "vlan" and data_vald[1] == "desc":
					for y in range(0, len(vlan_id)):
						if " to " in data[x]:
							i = int(data_vald[2])
							for i in range(int(data_vald[2]), int(data_vald[4])+1):
								if int(vlan_id[y]) == i:
									vlan_desc[y] = data_vald[6]
						elif int(vlan_id[y]) == int(data_vald[2]):
							vlan_desc[y] = data_vald[4]

				if data_vald[0] == "vlan" and data_vald[1] == "attrib":
					for y in range(0, len(vlan_id)):
						if " to " in data[x]:
							i = int(data_vald[2])
							for i in range(int(data_vald[2]), int(data_vald[4])+1):
								if int(vlan_id[y]) == i:
									vlan_attr[y] = data_vald[5]
						elif int(vlan_id[y]) == int(data_vald[2]):
							vlan_attr[y] = data_vald[3]
				if data_vald[0] == "port" and data_vald[1] == "vlan":
					for y in range(0, len(vlan_id)):
						if " to " in data[x]:
							i = int(data_vald[2])
							for i in range(int(data_vald[2]), int(data_vald[4])+1):
								if int(vlan_id[y]) == i:
									vlan_port[y] = str(data_vald[5])+ "/" +str(data_vald[6])
						elif int(vlan_id[y]) == int(data_vald[2]):
							vlan_port[y] = str(data_vald[3])+ "/" +str(data_vald[4])
			x = x+1

	db_input = []
	for i in range(0, len(vlan_id)):
		if vlan_id[i] not in db_input:
			result.append({
				"host"		:host,
				"index"		:vlan_id[i],
				"type"		:vlan_type[i],
				"desc"		:vlan_desc[i],
				"attrib"	:vlan_attr[i],
				"frame"		:vlan_port[i].split("/")[0],
				"slot"		:vlan_port[i].split("/")[1],
				"port"		:vlan_port[i].split("/")[2]
			})
			db_input.append(vlan_id[i])
	return result

def DbaProfile(host,data):
	result = []
	for x in range(0, len(data)):
		if data[x] != "":
			if "Profile-ID" in data[x]:
				x = x+1
			data_resp = re.sub(' +' , ' ' , data[x]).lstrip().split(" ")
			if data_resp:
				if len(data_resp) == 7:
					result.append({
						"host"		:host,
						"profile"	:data_resp[0],
						"type"		:data_resp[1],
						"bandwidth"	:data_resp[2],
						"fix"		:data_resp[3],
						"assure"	:data_resp[4],
						"max"		:data_resp[5],
						"bind"		:data_resp[6]
					})
	return result

def ServicePort(host,data):
	x = 0
	result = []
	while x < len(data):
		if data[x] != "":
			data_resp = re.sub(' +' , ' ' , data[x]).lstrip().split(" ")

			if "Total :" in data[x]:
				break
			if data_resp:
				if len(data_resp) >= 13 and len(data_resp) <= 14:
					if (data_resp[0] != "INDEX"):
						result.append({
							"host"		:host,
							"index"		:data_resp[0],
							"vlan"		:data_resp[1],
							"vlan_attr"	:data_resp[2],
							"type"		:data_resp[3],
							"frame"		:data_resp[4].split("/")[0],
							"slot"		:data_resp[4].split("/")[1],
							"port"		:data_resp[5].replace("/", ""),
							"vpi"		:data_resp[6],
							"vci"		:data_resp[7],
							"flow_type"	:data_resp[8],
							"flow_para"	:data_resp[9],
							"rx"		:data_resp[10],
							"tx"		:data_resp[11],
							"state"		:data_resp[12]
						})
		x = x+1
	return result

def GemPortProfile(host,id_lp,data):
	x = 0
	gemid = 0
	result = []
	lp_id = []
	while x < len(data):
		if data[x] != "":
			if "Gem Index" in data[x]:
				gemid = re.sub(' +' , ' ' , data[x]).lstrip().split(" ")[2].replace('>' , '')
			if "Mapping VLAN" in data[x]:
				while True:
					if x == len(data):
						break
					if "Gem Index" in data[x]:
						gemid = re.sub(' +' , ' ' , data[x]).lstrip().split(" ")[2].replace('>' , '')

					data_map = re.sub(' +' , ' ' , data[x]).lstrip().split(" ")
					# print(data_map)
					if len(data_map) == 9:
						if id_lp+ "-" +str(gemid)+ "-" +str(data_map[1]+ "-" +str(x)) not in lp_id:
							# print(data_map)
							result.append({
								"host"			:host,
								"profile"		:id_lp,
								"gem"			:gemid,
								"mapping"		:data_map[0],
								"vlan"			:data_map[1],
								"priority"		:data_map[2],
								"type"			:data_map[3],
								"port"			:data_map[4],
								"bundle"		:data_map[5],
								"flow"			:data_map[6],
								"transparent"	:data_map[7]
							})
							lp_id.append(id_lp+ "-" +str(gemid)+ "-" +str(data_map[1]+ "-" +str(x)))
					if "Binding" in data[x]:
						break
					x = x+1
		x = x+1
	return result

def OntInterface(host,data):
	result = []
	for x in range(0, len(data)):
		if data[x] != "" and x < len(data):
			data_map = re.sub(' +' , ' ' , data[x]).lstrip().split(" ")
			if data_map:
				if len(data_map) >= 9 and len(data_map) <= 10:
					if len(data_map) == 9 and (data_map[7] == "no" and data_map[8] == ""):
						result.append({
							"host"		:host,
							"frame"		:data_map[0].lstrip().split("/")[0],
							"slot"		:data_map[0].lstrip().split("/")[1],
							"port"		:data_map[0].lstrip().split("/")[2],
							"ont_id"	:data_map[1],
							"ont_sn"	:data_map[2],
							"flag"		:data_map[3],
							"run"		:data_map[4],
							"config"	:data_map[5],
							"match"		:data_map[6],
							"protect"	:data_map[7]
						})
					elif len(data_map) == 10 and (data_map[8] == "no" and data_map[9] == ""):
						result.append({
							"host"		:host,
							"frame"		:data_map[0].lstrip().split("/")[0],
							"slot"		:data_map[1].lstrip().split("/")[0],
							"port"		:data_map[1].lstrip().split("/")[1],
							"ont_id"	:data_map[2],
							"ont_sn"	:data_map[3],
							"flag"		:data_map[4],
							"run"		:data_map[5],
							"config"	:data_map[6],
							"match"		:data_map[7],
							"protect"	:data_map[8]
						})
	return result

def OntFindAll(host,data):
	prmNo = 0
	prmSn = ""
	prmVersion = ""
	prmSoftwareVersion = ""
	prmEquipmentId = ""
	prmFrameId = ""
	prmSlotId = ""
	prmPortId = ""
	result = []
	for x in range(0, len(data)):
		if data[x] != "":
			ont_list = ["Number","Ont SN","Ont Version","F/S/P","Ont EquipmentID","Ont SoftwareVersion"]
			if ont_list[0] in data[x]:
				prmNo = data[x].replace(ont_list[0],"").replace(":","").lstrip()
			elif ont_list[1] in data[x]:
				prmSn = (data[x].replace(ont_list[1],"").replace(":","").lstrip().split())[0]
			elif ont_list[2] in data[x]:
				prmVersion = data[x].replace(ont_list[2],"").replace(":","").lstrip()
			elif ont_list[3] in data[x]:
				data_fps = data[x].replace(ont_list[4],"").lstrip()
				data_arr = data_fps.split(":")[1].split("/")
				prmFrameId = data_arr[0].lstrip()
				prmSlotId = data_arr[1].lstrip()
				prmPortId = data_arr[2].lstrip()
			elif ont_list[4] in data[x]:
				prmEquipmentId = data[x].replace(ont_list[4],"").replace(":","").lstrip()
			elif ont_list[5] in data[x]:
				prmSoftwareVersion = data[x].replace(ont_list[5],"").replace(":","").lstrip()

			if prmSn !="" and prmVersion !="" and prmSoftwareVersion !="" and prmEquipmentId !="" and prmFrameId !="" and prmSlotId !="" and prmPortId !="":
				result.append({
					"host" 		:host,
					"ont_sn" 	:prmSn,
					"version" 	:prmVersion,
					"software" 	:prmSoftwareVersion,
					"equipment" :prmEquipmentId,
					"frame" 	:prmFrameId,
					"slot" 		:prmSlotId,
					"port" 		:prmPortId
				})
				prmNo = 0
				prmSn = ""
				prmVersion = ""
				prmSoftwareVersion = ""
				prmEquipmentId = ""
				prmFrameId = ""
				prmSlotId = ""
				prmPortId = ""

	return result

def SettingVlan(space,attribut,vlan,frame,slot,port,s_status):
	data = []
	# aktivasi vlan
	if s_status == 1 or s_status == '1':
		data.append(str(space)+'vlan '+str(vlan)+' smart')
		data.append(str(space)+'port vlan '+str(vlan)+ ' '+str(frame)+'/'+str(slot)+' '+str(port))
		if attribut != "Standard":
			data.append(str(space)+'vlan attrib '+str(vlan)+' stacking')
	return data

def SettingServiceProfile(space,srvprofile,vlan1,vlan2,vlan3,vlan4):
	data = []
	# aktivasi service profile
	data.append(str(space)+'ont-srvprofile gpon profile-id '+str(srvprofile))
	data.append(str(space)+'ont-port pots adaptive eth adaptive')
	if vlan1:
		data.append(str(space)+'port vlan eth 1 transparent')
	if vlan2:
		data.append(str(space)+'port vlan eth 2 transparent')
	if vlan3:
		data.append(str(space)+'port vlan eth 3 transparent')
	if vlan4:
		data.append(str(space)+'port vlan eth 4 transparent')
	data.append(str(space)+'commit')
	data.append(str(space)+'quit')
	return data

def SettingLineProfile(space,attribut,lnprofile,tcon,dba,gem,vlan,srvid,s_status):
	data = []
	# aktivasi line profile
	data.append(str(space)+'ont-lineprofile gpon profile-id '+str(lnprofile))
	if attribut == "QinQ":
		data.append(str(space)+'mapping-mode port')

	# aktivasi dba conection service
	data.append(str(space)+'tcont '+str(tcon)+' dba-profile-id '+str(dba))
	vln = []
	for i in range(0, len(gem)):
		# aktivasi service gem
		if s_status[i] == 1 or s_status[i] == "1":
			data.append(str(space)+'gem add '+str(gem[i])+' eth tcont 1')
			if attribut == "QinQ":
				eth = 1
				if int(gem[i]) > 0 and int(gem[i]) <= 8:
					eth = gem[i]
				data.append(str(space)+'gem mapping '+str(gem[i])+ ' 0 eth '+str(eth))
			else:
				data.append(str(space)+'gem mapping '+str(gem[i])+ ' 0 vlan '+str(vlan[i]))
				vln.append(vlan[i])
	data.append(str(space)+'commit')
	data.append(str(space)+'quit')
	return data

def SettingOntClient(space,desc,frame,slot,port,ont,sn,srv,lnp,vlan1,vlan2,vlan3,vlan4):
	data = []
	# activation ont clients
	data.append(str(space)+'interface gpon '+str(frame)+'/'+str(slot))
	data.append('ont add '+str(port)+' '+str(ont)+' sn-auth "'+str(sn)+'" omci ont-lineprofile-id '+ str(lnp)+' ont-srvprofile-id '+str(srv)+' desc "'+str(desc)+'"')
	if vlan1:
		data.append(str(space)+'ont port native-vlan '+str(port)+' '+ str(ont)+' eth 1 vlan '+str(vlan1)+' priority 0')
	if vlan1:
		data.append(str(space)+'ont port native-vlan '+str(port)+' '+ str(ont)+' eth 2 vlan '+str(vlan2)+' priority 0')
	if vlan1:
		data.append(str(space)+'ont port native-vlan '+str(port)+' '+ str(ont)+' eth 3 vlan '+str(vlan3)+' priority 0')
	if vlan1:
		data.append(str(space)+'ont port native-vlan '+str(port)+' '+ str(ont)+' eth 4 vlan '+str(vlan4)+' priority 0')
	data.append(str(space)+'quit')
	return data

def SettingServicePort(space,frame,slot,port,ont,gem,srvid,vlan,traffic,uservlan,innervlan,tag,sts):
	data = []
	# aktivasi service jika di setting
	traffic_table = ''
	inner_vlan = ''
	if traffic != None and traffic != 'None' and len(traffic) != 0:
		# aktivasi pembuatan traffic table jika di setting
		if sts:
			data.append(str(space)+'traffic table ip index '+str(traffic)+' cir '+str(int(traffic)*1024)+' pir '+str(int(traffic)*1024)+' priority 0 priority-policy local-setting')
		traffic_table =' inbound traffic-table index '+str(traffic)+' outbound traffic-table index '+str(traffic)
	if innervlan:
		inner_vlan =' inner-vlan '+str(innervlan)

	data.append(str(space)+'service-port '+str(srvid)+' vlan '+str(vlan)+' gpon '+str(frame)+'/'+str(slot)+'/'+str(port)+' ont '+str(ont)+' gemport '+str(gem)+ ' multi-service user-vlan '+str(uservlan)+ ' tag-transform '+str(tag)+''+inner_vlan+ ''+traffic_table)
	return data

def SettingClientsConfig(space,status,frame,slot,port,ont,sn):
	data = []
	# print(status)
	data.append(str(space)+'interface gpon '+str(frame)+'/'+str(slot))
	if status == "Active":
		data.append(str(space)+'ont activate '+str(port)+' '+str(ont))
	if status == "Postpone":
		data.append(str(space)+'ont deactivate '+str(port)+' '+str(ont))
	elif status == "Modify":
		data.append(str(space)+'ont modify '+str(port)+' '+str(ont) +' sn '+ str(sn))
	elif status == "Closed":
		data.append(str(space)+'ont delete '+str(port)+' '+str(ont))
	return data

def SettingServiceConfig(space,status,srv_id,traffic_new,traffic_old):
	data = []
	# modify bandwidth service
	if status == "Modify":
		data.append(str(space)+'service-port '+str(srv_id)+' inbound traffic-table index '+str(traffic_old)+' outbound traffic-table index '+str(traffic_new))
	elif status == "Closed":
		data.append(str(space)+'undo service-port '+str(srv_id))

	return data
