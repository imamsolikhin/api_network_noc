import os, re
from flask import Blueprint, request, jsonify
from flaskthreads import AppContextThread
from inspect import isclass
from pkgutil import iter_modules
from pathlib import Path
from importlib import import_module
from ....helper.utils import response_json, make_request
from ....helper import constants as cons
from ....library.inkombizz.telnet import Telnet

api = Blueprint('api', __name__, url_prefix=os.environ['API_URI']+"/huawei")
huawei = api

def Execute(cmd_list,hostname,username,password,sysname,line=None):
	start_sec = datetime.now().replace(microsecond=0)
	tn = Telnet(hostname=hostname,username=username,password=password,sysname=sysname)
	tn.login()
	data = tn.output
	if tn.status == False:
		data = []
		for ls in cmd_list:
			tn.cmd(ls)
			if ls not in ["enable","scroll 512","config"]:
				if line:
					data.append(tn.get_result_json(None, line))
				else:
					data.append(tn.get_result_json())

	tn.logout()
	end_sec = datetime.now().replace(microsecond=0)
	duration = end_sec-start_sec
	totalminute = int(duration.total_seconds())
	cons.status = tn.status;
	cons.message = tn.message;
	cons.message = str(totalminute)+" Second";
	cons.response = data;

# iterate through the modules in the current package
package_dir = Path(__file__).resolve().parent
for (_, module_name, _) in iter_modules([package_dir]):

	# import the module and iterate through its attributes
	module = import_module(f"{__name__}.{module_name}")
	for attribute_name in dir(module):
		attribute = getattr(module, attribute_name)

		if isclass(attribute):
			# Add the class to this package's variables
			globals()[attribute_name] = attribute
