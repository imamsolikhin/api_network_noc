"""
Views for the Api RND
"""

# third-party imports
import array as arr
from . import Telnet, Execute, AppContextThread, request, jsonify, cons, api, response_json, make_request
from datetime import datetime
from ...service.HuaweiService import TrafficTable, LineProfile, ServiceProfile, PortVlan, DbaProfile, ServicePort, GemPortProfile, OntInterface, OntFindAll

@api.route('/host/config')
def HostController():
	args	 = request.args
	auth	 = args.get('auth', default=None, type=str)
	config	 = args.getlist('config', type=str)
	hostcode = args.get('hostcode', default=None, type=str)
	hostname = args.get('hostname', default=None, type=str)
	username = args.get('username', default=None, type=str)
	password = args.get('password', default=None, type=str)
	sysname	 = args.get('sysname', default=None, type=str)
	lnprofile= args.get('lnprofile', default="", type=str)

	start_sec = datetime.now().replace(microsecond=0)
	tn = Telnet(hostname=hostname,username=username,password=password,sysname=sysname)
	tn.login()
	data = tn.output
	if tn.status == False and auth != None:
		data = []
		temp = []
		tn.cmd('enable')
		tn.cmd('scroll 512')
		tn.cmd('config')
		if "check-host" in config:
			data.append(tn.get_result_json())

		if "current-config" in config:
			tn.cmd('display current-configuration')
			data.append(["current-config",hostcode,tn.get_result_json()])

		if "traffic-table" in config or "all" in config:
			tn.cmd('display current-configuration section global-config')
			data.append(["traffic-table",hostcode,TrafficTable(hostcode,tn.get_result_json())])

		if "line-profile" in config or "all" in config:
			tn.cmd('display ont-lineprofile gpon all')
			data.append(["line-profile",hostcode,LineProfile(hostcode,tn.get_result_json())])

		if "srv-profile" in config or "all" in config:
			tn.cmd('display ont-srvprofile gpon all')
			data.append(["srv-profile",hostcode,ServiceProfile(hostcode,tn.get_result_json())])

		if "port-vlan" in config or "all" in config:
			tn.cmd('display current-configuration section vlan-config')
			data.append(["port-vlan",hostcode,PortVlan(hostcode,tn.get_result_json())])

		if "dba-profile" in config or "all" in config:
			tn.cmd('display dba-profile all')
			data.append(["dba-profile",hostcode,DbaProfile(hostcode,tn.get_result_json())])

		if "service-port" in config or "all" in config:
			tn.cmd('display service-port all')
			data.append(["service-port",hostcode,ServicePort(hostcode,tn.get_result_json())])

		if "gem-port" in config or "all" in config:
			result = []
			for lp in lnprofile.split(','):
				tn.cmd('display ont-lineprofile gpon profile-id ' + str(lp))
				result += GemPortProfile(hostcode,lp,tn.get_result_json())
			data.append(["gem-port",hostcode,result])

		if "ont-info" in config or "all" in config:
			tn.cmd('display ont info 0 all')
			data.append(["ont-info",hostcode,OntInterface(hostcode,tn.get_result_json())])

		if "ont-autofind" in config or "all" in config:
			tn.cmd('display ont autofind all')
			data.append(["ont-autofind",hostcode,OntFindAll(hostcode,tn.get_result_json())])

	end_sec = datetime.now().replace(microsecond=0)
	duration = end_sec-start_sec
	totalminute = int(duration.total_seconds())
	print("Total Time = "+str(totalminute)+" Second")
	cons.message = str(totalminute)+" Second"
	cons.response = data
	return response_json(cons.status,cons.message,cons.response)
