"""
Views for the Api RND
"""

# third-party imports
from . import Telnet, Execute, AppContextThread, request, jsonify, cons, api, response_json, make_request
from datetime import datetime

@api.route('/ont/autofind-all')
def findall():
	args	 = request.args
	auth	 = args.get('auth', default=None, type=str)
	hostname = args.get('hostname', default=None, type=str)
	username = args.get('username', default=None, type=str)
	password = args.get('password', default=None, type=str)
	sysname	 = args.get('sysname', default=None, type=str)

	cmd_list = []
	cmd_list.append("enable")
	cmd_list.append("scroll 512")
	cmd_list.append("config")
	cmd_list.append("display ont autofind all")

	cons.message = "Command Script"
	cons.response = cmd_list
	if auth:
		thread = AppContextThread(target=Execute(cmd_list,hostname,username,password,sysname))
		thread.start()
		thread.join()
	return response_json(cons.status,cons.message,cons.response)

@api.route('/ont/check-power')
def checkpower():
	args	 = request.args
	auth	 = args.get('auth', default=None, type=str)
	hostname = args.get('hostname', default=None, type=str)
	username = args.get('username', default=None, type=str)
	password = args.get('password', default=None, type=str)
	port 	 = args.get('port', default=None, type=str)
	sysname	 = args.get('sysname', default=None, type=str)
	ontsn	 = args.get('ontsn', default=None, type=str)
	ontid	 = args.get('ontid', default=None, type=str)
	frameid	 = args.get('frameid', default=None, type=str)
	slotid	 = args.get('slotid', default=None, type=str)
	portid	 = args.get('portid', default=None, type=str)

	cmd_list = []
	cmd_list.append("enable")
	cmd_list.append("scroll 512")
	cmd_list.append("config")
	cmd_list.append('interface gpon '+str(frameid)+'/'+str(slotid))
	if ontid:
		cmd_list.append('display ont info '+str(portid)+' '+str(ontid))
		cmd_list.append('display ont optical-info '+str(portid)+' '+str(ontid))
		cmd_list.append('quit')
	else:
		cmd_list.append('ont add '+str(portid)+ ' 127 sn-auth "'+str(ontsn)+'" omci desc "Add Test"')
		cmd_list.append('display ont info '+str(portid)+' '+str(ontid))
		cmd_list.append('display ont optical-info '+str(portid)+' '+str(ontid))
		cmd_list.append('ont delete '+str(portid)+' '+str(ontid))
		cmd_list.append('quit')

	cons.message = "Command Script"
	cons.response = cmd_list

	if auth:
		thread = AppContextThread(target=Execute(cmd_list,hostname,username,password,port,sysname,2,100))
		thread.start()
		thread.join()
	return response_json(cons.status,cons.message,cons.response)
