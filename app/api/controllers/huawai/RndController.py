"""
Views for the Api RND
"""

# third-party imports
from . import Telnet, AppContextThread, request, jsonify, cons, api, response_json, make_request
from datetime import datetime
# from ..models.Host import Host

@api.route('/rnd')
def rnd():
	args	 = request.args
	hostname = args.get('hostname', default=None, type=str)
	username = args.get('username', default=None, type=str)
	password = args.get('password', default=None, type=str)
	port 	 = args.get('port', default=None, type=str)
	sysname	 = args.get('sysname', default=None, type=str)
	thread = AppContextThread(target=execute(hostname,username,password,port,sysname))
	thread.start()
	thread.join()
	return response_json(cons.status,cons.message,cons.response)

def execute(hostname,username,password,port,sysname):
	start_sec = datetime.now().replace(microsecond=0)
	tn = Telnet(hostname=hostname,username=username,password=password,port=port,sysname=sysname)
	tn.login()
	tn.cmd("enable")
	tn.cmd("scroll 512")
	tn.cmd("config")
	tn.cmd("display current-configuration")
	tn.cmd("display ont autofind all")
	data = tn.get_result_json()
	tn.logout()

	end_sec = datetime.now().replace(microsecond=0)
	duration = end_sec-start_sec
	totalminute = int(duration.total_seconds())
	cons.status = tn.status;
	cons.message = tn.message;
	cons.message = str(totalminute)+" Second";
	cons.response = data;
