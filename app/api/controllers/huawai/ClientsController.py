"""
Views for the Api RND
"""

# third-party imports
import array as arr
from . import Telnet, Execute, AppContextThread, request, jsonify, cons, api, response_json, make_request
from datetime import datetime
from ...service.HuaweiService import SettingVlan, SettingServiceProfile, SettingLineProfile, SettingOntClient, SettingServicePort, SettingClientsConfig, SettingServiceConfig

@api.route('/clients/setting')
def ClientsController():
	start_sec = datetime.now().replace(microsecond=0)
	args	 = request.args
	auth	 = args.get('auth', default="script", type=str)
	config	 = args.getlist('config', type=str)
	hostcode = args.get('hostcode', default=None, type=str)
	hostname = args.get('hostname', default=None, type=str)
	username = args.get('username', default=None, type=str)
	password = args.get('password', default=None, type=str)
	sysname	 = args.get('sysname', default=None, type=str)

	desc	= args.get('desc', default=None, type=str)
	status	= args.get('status', default=None, type=str)

	bframe	= args.get('bframe', default="", type=str)
	bslot	= args.get('bslot', default="", type=str)
	bport	= args.get('bport', default="", type=str)

	frame	= args.get('frame', default=None, type=str)
	slot	= args.get('slot', default=None, type=str)
	port	= args.get('port', default=None, type=str)
	ont		= args.get('ont', default=None, type=str)
	sn		= args.get('sn', default=None, type=str)

	srvprofile	= args.get('srvprofile', default=None, type=str)
	lnprofile	= args.get('lnprofile', default=None, type=str)
	attribut	= args.get('attribut', default=None, type=str)

	vlan		= args.get('vlan', default=None, type=str)
	vlan1		= args.get('vlan1', default=None, type=str)
	vlan2		= args.get('vlan2', default=None, type=str)
	vlan3		= args.get('vlan3', default=None, type=str)
	vlan4		= args.get('vlan4', default=None, type=str)

	tcon	= args.get('tcon', default=1, type=str)
	dba		= args.get('dba', default=10, type=str)

	s_status	= args.getlist('s_status', type=None)
	srvid		= args.getlist('srvid', type=None)
	gem			= args.getlist('gem', type=None)
	svlan		= args.getlist('svlan', type=None)
	uservlan	= args.getlist('uservlan', type=None)
	innervlan	= args.getlist('innervlan', type=None)
	tag			= args.getlist('tag', type=None)
	traffic		= args.getlist('traffic', type=None)
	traffic_new = args.getlist('traffic_new', type=None)
	traffic_old = args.getlist('traffic_old', type=None)
	eth			= args.getlist('eth', type=None)

	space = " "
	data = []

	# Starting Setting fast load
	if auth != "script":
		space = ""

	# aktivasi vlan
	if "setting-vlan" in config:
		if auth == "script":
			data += ['']
			data += ['#Setting vlan#']
			data += ['==============']
		check_vlan = []
		for i in range(0, len(svlan)):
			if svlan[i] not in check_vlan:
				print(s_status[i])
				data += SettingVlan(space,attribut,svlan[i],bframe,bslot,bport,s_status[i])
				check_vlan.append(svlan[i])

	# aktivasi service profile
	if "setting-service-profile" in config:
		if auth == "script":
			data += ['']
			data += ['#Setting service profile#']
			data += ['=========================']
		data += SettingServiceProfile(space,srvprofile,vlan1,vlan2,vlan3,vlan4)

	# aktivasi line profile
	if "setting-line-profile" in config:
		if auth == "script":
			data += ['']
			data += ['#Setting line profile#']
			data += ['======================']
		data += SettingLineProfile(space,attribut,lnprofile,tcon,dba,gem,uservlan,srvid,s_status)

	# activation ont clients
	if "setting-ont" in config:
		if auth == "script":
			data += ['']
			data += ['#Setting ont clients#']
			data += ['=====================']
		data += SettingOntClient(space,desc,frame,slot,port,ont,sn,srvprofile,lnprofile,vlan1,vlan2,vlan3,vlan4)

	# aktivasi service port
	if "setting-service-port" in config:
		if auth == "script":
			data += ['']
			data += ['#Setting service port#']
			data += ['======================']
			
		trf = []
		for i in range(0, len(tag)):
			if traffic[i] not in trf:
				data += SettingServicePort(space,frame,slot,port,ont,gem[i],srvid[i],svlan[i],traffic[i],uservlan[i],innervlan[i],tag[i],True)
				trf.append(traffic[i])
			else:
				data += SettingServicePort(space,frame,slot,port,ont,gem[i],srvid[i],svlan[i],traffic[i],uservlan[i],innervlan[i],tag[i],False)

	# config service port
	if "setting-traffic" in config:
		if auth == "script" and len(srvid) > 0 and (status == "Closed" or status == "Modify"):
			data += ['']
			data += ['#Setting service '+str(status)+'#']
			data += ['==============']
		for i in range(0, len(srvid)):
			if status == "Modify":
				if traffic_new[i] != "None":
					data.append(str(space)+'traffic table ip index '+str(traffic_new[i])+' cir '+str(int(traffic_new[i])*1024)+' pir '+str(int(traffic_new[i])*1024)+' priority 0 priority-policy local-setting')
				data += SettingServiceConfig(space,status,srvid[i],traffic_new[i],traffic_new[i])
			else:
				data += SettingServiceConfig(space,status,srvid[i],None,None)
	# config clients
	if "setting-clients" in config:
		if auth == "script":
			data += ['']
			data += ['#Setting clients '+str(status)+'#']
		data += SettingClientsConfig(space,status,frame,slot,port,ont,sn)

	# Starting Setting fast load
	if auth != "script":
		space = ""
		tn = Telnet(hostname=hostname,username=username,password=password,sysname=sysname)
		tn.login()
		tn.cmd('enable',0.1)
		tn.cmd('scroll 512',0.1)
		tn.cmd('config',0.1)
		# print(data)
		data = runCommand(tn,data)
		tn.logout()
	end_sec = datetime.now().replace(microsecond=0)
	duration = end_sec-start_sec
	totalminute = int(duration.total_seconds())
	cons.message = str(totalminute)+" Second"
	cons.response = data
	return response_json(cons.status,cons.message,cons.response)

def runCommand(tn,cmd_list):
	data = []
	for ls in cmd_list:
		tn.cmd(ls)
		datas = tn.get_result_json()
		data += datas
		# print(datas)
	return data
