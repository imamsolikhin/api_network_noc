"""
Views for the Api RND
"""

# third-party imports
import time
import array as arr
from . import Telnet, Execute, AppContextThread, request, jsonify, cons, api, response_json, make_request
from datetime import datetime
from ...service.HuaweiService import CheckPower, OntOpticalInfo, OntInfo, OntWanInfo, OntPortState, TrafficTable, LineProfile, ServiceProfile, PortVlan, DbaProfile, ServicePort, GemPortProfile, OntInterface, OntFindAll

@api.route('/clients/tools')
def ClientsToolsController():
	args	 = request.args
	auth	 = args.get('auth', default=None, type=str)
	config	 = args.get('config', default=None, type=str)
	hostcode = args.get('hostcode', default=None, type=str)
	hostname = args.get('hostname', default=None, type=str)
	username = args.get('username', default=None, type=str)
	password = args.get('password', default=None, type=str)
	sysname	 = args.get('sysname', default=None, type=str)

	frame	= args.get('frame', default=None, type=str)
	slot	= args.get('slot', default=None, type=str)
	port	= args.get('port', default=None, type=str)
	ont		= args.get('ont', default=None, type=str)
	sn		= args.get('sn', default=None, type=str)
	mode	= args.get('mode', default=None, type=str)

	start_sec = datetime.now().replace(microsecond=0)
	tn = Telnet(hostname=hostname,username=username,password=password,sysname=sysname)
	tn.login()
	data = tn.output
	if tn.status == False and auth != None:
		end_sec = datetime.now().replace(microsecond=0)
		duration = end_sec-start_sec
		totalminute = int(duration.total_seconds())
		data = []
		tn.cmd('enable',0.3)
		tn.cmd('scroll 512',0.3)
		tn.cmd('config',0.3)
		if config=='ont-info' or config =='all' or config =='general-info':
			tn.cmd('display ont info '+str(frame)+' '+str(slot)+' '+str(port)+' '+str(ont)+'')

			data.append(["ont-info",tn.get_result_json()])
			if config =='general-info':
				data.append(["ont-info-model",OntInfo(hostcode,tn.get_result_json())])

		if config =='wan-info' or config =='all' or config =='general-info':
			response = []
			tn.cmd('display ont wan-info '+str(frame)+'/'+str(slot)+' '+str(port)+' '+str(ont))

			data.append(["wan-info",tn.get_result_json()])
			if config =='general-info':
				data.append(["wan-info-model",OntWanInfo(hostcode,tn.get_result_json())])

		if config =='port-state' or config == 'all' or config =='general-info':
			response = []
			tn.cmd('interface gpon '+str(frame)+'/'+str(slot))
			tn.cmd('display ont port state '+str(port)+' '+str(ont)+' eth-port all')

			data.append(["port-state",tn.get_result_json()])
			if config =='general-info':
				data.append(["port-state-model",OntPortState(hostcode,tn.get_result_json())])
			tn.cmd('quit')

		if config =='optical-info' or config =='all' or config =='general-info':
			response = []
			tn.cmd('interface gpon '+str(frame)+'/'+str(slot))
			tn.cmd('display ont optical-info '+str(port)+' '+str(ont)+'')
			data.append(["optical-info",tn.get_result_json()])
			if config =='general-info':
				data.append(["optical-info-model",OntOpticalInfo(hostcode,tn.get_result_json())])

		if config =='check-power':
			tn.cmd('interface gpon '+str(frame)+'/'+str(slot),0.2)
			if ont == None or ont == '' or ont == ' ':
				ont = 127
				tn.cmd('ont add '+str(port)+ ' '+str(ont)+' sn-auth "'+str(sn)+'" omci',0.5)

			tn.cmd('display ont optical-info '+str(port)+' '+str(ont),0.5)
			rs = tn.get_result_json(None,20)
			if "  Failure: The ONT is not online" in rs:
			# if any("not online" in s for s in rs):
				rs = ClientsSignal(hostname,username,password,sysname,frame,slot,port,ont,sn)

			tn.cmd('display ont info '+str(port)+' '+str(ont),0.5)
			rs += tn.get_result_json(None,20)

			if ont == 127 or ont == "127":
				tn.cmd('ont delete '+str(port)+' '+str(ont))
			data.append(["check-power",CheckPower(hostcode,rs)])
			end_sec = datetime.now().replace(microsecond=0)
			duration = end_sec-start_sec
			totalminute = int(duration.total_seconds())
			# print("Total Time : "+str(totalminute)+" Second")
			tn.cmd('quit')

		if config=='ont-version' or config =='all':
			tn.cmd('display ont version '+str(frame)+' '+str(slot)+' '+str(port)+' '+str(ont)+'')
			data.append(["ont-version",tn.get_result_json()])

		if config =='register-info' or config =='all':
			response = []
			tn.cmd('interface gpon '+str(frame)+'/'+str(slot))
			tn.cmd('display ont register-info '+str(port)+' '+str(ont)+' ')
			data.append(["register-info",tn.get_result_json()])

		if config =='mac-address' or config =='all':
			response = []
			tn.cmd('display mac-address port '+str(frame)+'/'+str(slot)+'/'+str(port)+' ont '+str(ont))
			data.append(["mac-address",tn.get_result_json()])

		if config =='port-attribute' or config =='all':
			response = []
			tn.cmd('interface gpon '+str(frame)+'/'+str(slot))
			tn.cmd('display ont port attribute '+str(port)+' '+str(ont)+' eth')
			data.append(["port-attribute",tn.get_result_json()])

		if config =='fec-crc-error' or config =='all':
			response = []
			tn.cmd('interface gpon '+str(frame)+'/'+str(slot))
			tn.cmd('port '+str(port)+' fec enable')
			tn.cmd('display statistics ont-line-quality '+str(port)+' '+str(ont))
			data.append(["fec-crc-error",tn.get_result_json()])
	# tn.logout()
	end_sec = datetime.now().replace(microsecond=0)
	duration = end_sec-start_sec
	totalminute = int(duration.total_seconds())
	cons.message = str(totalminute)+" Second"
	cons.response = data
	return response_json(cons.status,cons.message,cons.response)

def ClientsSignal(hostname,username,password,sysname,frame,slot,port,ont,sn,i=5):
	start_sec = datetime.now().replace(microsecond=0)
	tn = Telnet(hostname=hostname,username=username,password=password,sysname=sysname)
	tn.login()

	end_sec = datetime.now().replace(microsecond=0)
	duration = end_sec-start_sec
	totalminute = int(duration.total_seconds())
	print("")
	print("==============================")
	print("Server "+str(i)+" Start ==> "+str(totalminute)+" Second")

	tn.cmd('enable',0.1)
	tn.cmd('scroll 512',0.1)
	tn.cmd('config',0.1)
	tn.cmd('interface gpon '+str(frame)+'/'+str(slot),1)

	tn.cmd('display ont optical-info '+str(port)+' '+str(ont),1)
	rs = tn.get_result_json(None,20)
	# if "not online" in rs:
	if any("The ONT is not online" in s for s in rs) and int(i) != 0:
		tn.logout()
		x = i-1
		time.sleep(2)
		rs = ClientsSignal(hostname,username,password,sysname,frame,slot,port,ont,sn,x)
		# if int(x) < 1:
		# 	rs = ClientsSignal(hostname,username,password,sysname,frame,slot,port,ont,sn,x)
	return rs
