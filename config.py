"""
Configuration file for the application.
"""
#local import
import os

basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
	"""
	Common configurations
	"""
	# Put any configurations here that are common across all environments
	SECRET_KEY = os.environ['SECRET_KEY']

class ProductionConfig(Config):
	DEBUG = False


class StagingConfig(Config):
	DEBUG = True


class DevelopmentConfig(Config):
	DEBUG = True
