FROM python:3.9-slim-buster
USER root

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV FLASK_ENV=production
ENV API_URI=/api/v1
ENV FLASK_APP=run.py
ENV SECRET_KEY=\x9c\xf5\xba\xd2Q\x9aP\x17\x8b\x8e9\xae5$\x08\xa0

# install nginx
RUN apt-get update && apt-get install nginx vim -y --no-install-recommends
COPY deploy/nginx.default /etc/nginx/sites-available/default
COPY deploy/nginx.conf /etc/nginx/nginx.conf

RUN ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log

# copy source and install dependencies
RUN mkdir -p /var/www/app
WORKDIR /var/www/app
COPY . /var/www/app

ENV TZ=Asia/Jakarta
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN mkdir -p /var/log/cron/
RUN mkdir -p /var/log/supervisor/
RUN mkdir -p /var/log/supervisor-error/
COPY deploy/supervisord.ini /etc/supervisor.d/supervisord.ini

RUN pip install -r reqs.txt --cache-dir /var/www/app/pip_cache
#RUN adduser  --no-create-home  --system  --user-group --shell /bin/false   www-data
RUN chown -R www-data:www-data /var/www/app
RUN chmod 755 start-server.sh

# start server
EXPOSE 8004
STOPSIGNAL SIGTERM
CMD ["supervisord", "-c", "/etc/supervisor.d/supervisord.ini"]
CMD ["/var/www/app/start-server.sh"]
