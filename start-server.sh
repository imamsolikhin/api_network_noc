#!/usr/bin/env bash
# start-server.sh
if [ -n "$DJANGO_SUPERUSER_USERNAME" ] && [ -n "$DJANGO_SUPERUSER_PASSWORD" ] ; then
    (python manage.py createsuperuser --no-input)
fi
(gunicorn run:app --user www-data --bind 0.0.0.0:8004 --workers 3 --timeout 600 --threads 100 --worker-connections 1000) &
nginx -g "daemon off;"
