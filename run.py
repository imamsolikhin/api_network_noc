from config import DevelopmentConfig, ProductionConfig, StagingConfig
from app import create_app
from rq import Worker, Queue, Connection
import redis
import os

redis_url = 'redis://172.16.32.150:6379'
conn = redis.from_url(redis_url)

app = create_app(DevelopmentConfig)

if __name__ == '__main__':
	with Connection(conn):
		worker = Worker(list(map(Queue, listen)))
		worker.work()
	app.run(threaded=True)
